#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.05

// physical constants
// original version: 03-05-23 by mm
// $Id$

// source: CODATA 2002 [Rev. Mod. Phys. 77, 1 (2005)]

// universal constants

constant kSpeedOfLight = 2.99792458e8 // m/s
constant kMagnField = 1.25663706144e-6 // V s / A / m
constant kElField = 8.854187817620e-12 // A s / V / m

constant kGravitation = 6.6742e-11 // m^3 / kg / s^2

constant kHBar =  6.58211915e-16 // eV s
constant kPlanckBar = 6.58211915e-16 // eV s
constant kPlanck = 4.13566743e-15 // eV s
constant kHBarC = 197.326968 // MeV fm
constant kHC = 1239.84190605 // eV nm
constant kHCicm = 1239.84190605e-7 // eV cm^-1
constant kPlanckBarSI = 1.05457168e-34 // J s
constant kPlanckSI = 6.6260693e-34 // J s

// electromagnetic constants

constant kElCharge = 1.60217653e-19 // A s
constant kMagnFlux = 2.06783372e-15 // Wb
constant kConductance = 7.748091733e-5 // S

constant kBohrMagneton = 5.788381804e-5 // eV T^-1
constant kBohrMagnetonSI = 9.27400949e-24 // J T^-1 = A m^2
constant kNuclearMagneton = 3.152451259e-8 // eV T^-1
constant kNuclearMagnetonSI = 5.05078343e-27 // J T^-1

// atomic and nuclear constants

constant kFineStruct = 7.297352568e-3
constant kInvFineStruct = 137.03599911

constant kRydberg = 10973731.568525 // m^-1
constant kRydbergEnergy = 13.6056923 // eV
constant kBohrRadius = 0.5291772108e-10 // m
constant kHartreeEnergy = 27.2113845 // eV
constant kHartreeEnergySI = 4.35974417 // J

constant kElectronMass = 510.998918e3 // eV c^-2
constant kMuonMass = 105.6583692e6 // eV c^-2
constant kProtonMass = 938.272029e6 // eV c^-2
constant kNeutronMass = 939.565360e6 // eV c^-2
constant kElectronMassSI = 9.1093826e-31 // kg
constant kProtonMassSI = 1.67262171e-27 // kg

constant kComptonWavelength = 2.426310238e-12 // m
constant kElectronRadius = 2.817940325e-15 // m
constant kThomsonCrossSection = 0.665245873e-28 // m^2
constant kElectronGFactor = -2.0023193043718

// physico-chemical constants

constant kAvogadro = 6.0221415e23 // 1 / mol

constant kAtomicMassUnit = 931.494043e6 // eV / c^2
constant kAtomicMassUnitSI = 1.66053886e-27 // kg

constant kMolarGasSI = 8.314472 // J / K / mol
constant kBoltzmann = 8.617343e-5 // eV / K
constant kBoltzmannSI = 1.3806505e-23 // J /K
constant kWien = 2.8977685e-3 // m K
constant kStefanBoltzmann = 5.670400e-8 // W m^-2 K^-4

constant kJoulesPerEV = 1.60217653e-19 // J / eV
constant kEVPerHartree = 27.2113845 // eV / Eh

// custom constants

constant kFreeElectronDispersion = 3.79736 // eV Angstrom^2
	// = h_bar^2 * c^2 / (2 * m_e)
	// for E = kFreeElectronDispersion * k^2

threadsafe function FreeElectronWavelength(ekin, [v0, meff])
	// Wavelength of a quasi-free electron in meters
	variable ekin	// kinetic energy of the electron in eV
	variable v0	// inner potential (where applicable), default = 0
	variable meff	// effective mass relative to free electron, default = 1
	
	if (ParamIsDefault(v0))
		v0 = 0
	endif
	if (ParamIsDefault(meff))
		meff = 1
	endif
	
	return khc * 1e-9 / sqrt(2 * kElectronMass * meff * (ekin + v0))
end
