#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=1		// Use modern global access method.
#pragma ModuleName = PearlMenu
#pragma version = 1.02

// main menu for PEARL data acquisition and analysis packages

// $Id$
// author: matthias.muntwiler@psi.ch
// Copyright (c) 2013-14 Paul Scherrer Institut

// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0

menu "PEARL"
	
	submenu "Data Files"
		PearlMenuEnableFunc("pearl_data_explorer") + "Data Explorer", /Q, pearl_data_explorer()
		help = {"Data explorer panel with file import and preview", "Requires ARPES package and HDF5 XOP"}
		PearlMenuEnableFunc("ad_load_dialog") + "AD HDF5", /Q, ad_load_dialog("")
		help = {"Import area detector HDF5 data file", "Requires ARPES package and HDF5 XOP"}
	end
	
	submenu "On-the-Fly Data"
		PearlMenuEnableFunc("otf_rename_folders") + "Shorten OTF Folder Names", /Q, otf_rename_folders("010")
		help = {"Renames otf_xxxxxx_yyyyyy_zzzz data folders to otf_yyyyyy (removing date and suffix)", "Requires Optics package"}
		PearlMenuEnableFunc("otf_gather_batch") + "Gather OTF Batch", /Q, otf_gather_batch("current_ch1", "photonenergy", "otf_batch")
		help = {"Copies data from all otf_* folders into otf_batch folder", "Requires Optics package"}
		PearlMenuEnableFunc("PearlOpticsPreviewPanel") + "OTF Preview", /Q, PearlOpticsPreviewPanel()
		help = {"Opens a preview panel for OTF data in otf_* folders", "Requires Optics package"}
	end

	submenu "Scienta Analyser"
		PearlMenuEnableFunc("ad_display_profiles") + "Scienta Live View", /Q, PearlLiveDisplay("X03DA-SCIENTA:", "EA", "(65280,54528,48896)")
		help = {"Display preview panel with latest image from Scienta", "Requires ARPES package and EPICS XOP"}
		PearlMenuEnableFunc("ast_setup") + "Angle Scan Tracker", /Q, PearlAnglescanTracker("X03DA-SCIENTA:", "(65280,54528,48896)")
		help = {"Preview of acquired angle scan data and current detection angles.", "Requires ARPES package and EPICS XOP"}
		PearlMenuEnableFunc("sample_tracker") + "Sample Tracker", /Q, PearlSampleTracker(1)
		help = {"Live tracking and adjustment of sample position.", "Requires ARPES package and EPICS XOP"}
	end
	
	submenu "Cameras"
		PearlMenuEnableFunc("ad_display_profiles") + "Exit Slit Camera", /Q, PearlLiveDisplay("X03DA-OP-PS1:", "OP", "(65280,54528,48896)")
		help = {"Display preview panel with latest image from Scienta", "Requires ARPES package and EPICS XOP"}
		PearlMenuEnableFunc("ad_display_profiles") + "Manipulator Camera", /Q, PearlCameraDisplay("X03DA-ES-PS1:", "ES", "(32767,32767,32767)")
		help = {"Display live panel of the exit slit camera", "Requires ARPES package and EPICS XOP"}
	end
	
	submenu "Display"
		PearlMenuEnableFunc("ad_display_profiles") + "2D Profiles", /Q, Display2dProfiles()
		help = {"Profiles display of 2D data", "Requires ARPES package"}
		PearlMenuEnableFunc("ad_display_brick") + "3D Slicer", /Q, Display3dSlicer()
		help = {"Slice and profiles display of 3D data", "Requires ARPES package"}
		PearlMenuEnableFunc("ad_display_brick") + "3D Gizmo", /Q, DisplayGizmoSlicer()
		help = {"Gizmo display of 3D data", "Requires ARPES package"}
	end
	
	submenu "Process"
		PearlMenuEnableFunc("asp_show_panel") + "XPD scans", /Q, asp_show_panel()
		help = {"Data processing of two-pi angle scans", "Requires ARPES package"}
	end
	
	submenu "Services"
		PearlMenuEnableFunc("pearl_elog") + "Open ELOG Panel", /Q, pearl_elog("")
		help = {"Open an ELOG panel to send entries to an ELOG logbook"}
	end
	
	submenu "Sample Preparation"
		PearlMenuEnableFunc("ramp_generator") + "Annealing Ramp", /Q, ramp_generator()
		help = {"Sample annealing ramp generator"}
	end
	
	submenu "Packages"
		"Load ARPES Package", /Q, LoadPearlArpes()
		help = {"Data processing and analysis for ARPES experiments"}
		"Load Preparation Package", /Q, LoadPearlPreparation()
		help = {"Process control for sample preparation"}
		"Load Optics Package", /Q, LoadPearlOptics()
		help = {"Data processing and analysis for beamline commissioning"}
	end
end

/// check whether a function name exists
///
/// return a prefix which disables the menu item
/// if the function does not exist
///
function /s PearlMenuEnableFunc(funcname)
	string funcname
	if (exists(funcname) >= 3)
		return ""
	else
		return "("
	endif
end

function LoadPearlOptics()
	execute /p/q/z "INSERTINCLUDE \"pearl-optics\""
	execute /p/q/z "COMPILEPROCEDURES "
	execute /p/q/z "BuildMenu \"PEARL\""
end

function LoadPearlArpes()
	execute /p/q/z "INSERTINCLUDE \"pearl-arpes\""
	execute /p/q/z "COMPILEPROCEDURES "
	execute /p/q/z "BuildMenu \"PEARL\""
end

function LoadPearlPreparation()
	execute /p/q/z "INSERTINCLUDE \"pearl-preparation\""
	execute /p/q/z "COMPILEPROCEDURES "
	execute /p/q/z "BuildMenu \"PEARL\""
end

function Display2dProfiles()
	dfref dfBefore = GetDataFolderDFR()
	Execute /q/z "CreateBrowser prompt=\"Select 2D wave\", showWaves=1, showVars=0, showStrs=0"
	dfref dfAfter = GetDataFolderDFR()
	SetDataFolder dfBefore
	
	SVAR list = S_BrowserList
	NVAR flag = V_Flag
	
	if ((flag != 0) && (ItemsInList(list) >= 1))
		string brickname = StringFromList(0, list)
		string cmd
		sprintf cmd, "ad_display_profiles(%s)", brickname
		execute /q/z cmd
	endif
end

function Display3dSlicer()
	dfref dfBefore = GetDataFolderDFR()
	Execute /q/z "CreateBrowser prompt=\"Select 3D wave\", showWaves=1, showVars=0, showStrs=0"
	dfref dfAfter = GetDataFolderDFR()
	SetDataFolder dfBefore
	
	SVAR list = S_BrowserList
	NVAR flag = V_Flag
	
	if ((flag != 0) && (ItemsInList(list) >= 1))
		string brickname = StringFromList(0, list)
		string cmd
		sprintf cmd, "ad_display_slice(%s)", brickname
		execute /q/z cmd
		sprintf cmd, "ad_brick_slicer(%s)", brickname
		execute /q/z cmd
	endif
end

function DisplayGizmoSlicer()
	dfref dfBefore = GetDataFolderDFR()
	Execute /q/z "CreateBrowser prompt=\"Select 3D wave\", showWaves=1, showVars=0, showStrs=0"
	dfref dfAfter = GetDataFolderDFR()
	SetDataFolder dfBefore
	
	SVAR list = S_BrowserList
	NVAR flag = V_Flag
	
	if ((flag != 0) && (ItemsInList(list) >= 1))
		string brickname = StringFromList(0, list)
		string cmd
		sprintf cmd, "ad_display_brick(%s)", brickname
		execute /q/z cmd
		sprintf cmd, "ad_brick_slicer(%s)", brickname
		execute /q/z cmd
	endif
end

/// area detector live display
///
/// display an area detector channel in an ad_display_profiles() window.
///
/// @param	epicsname			base name of the detector, e.g. X03DA-SCIENTA:
///									image1: and cam1: are appended by the function.
///									see ad_connect().
///
/// @param	nickname			nick name under which this detector is referred to in Igor.
/// 								must be a valid name for a data folder.
///									see ad_connect().
///
/// @param	wbRGB				window background color, e.g. "(32768,49152,55296)"
///
function PearlLiveDisplay(epicsname, nickname, wbRGB)
	string epicsname
	string nickname
	string wbRGB
	
	string cmd
	sprintf cmd, "ad_connect(\"%s\", \"%s\")", epicsname, nickname
	execute /q/z cmd
	sprintf cmd, "ad_display_profiles(root:pearl_epics:%s:image)", nickname
	execute /q/z cmd
	//sprintf cmd, "ad_add_overlay(root:pearl_epics:%s:image)", nickname
	//execute /q/z cmd
	sprintf cmd, "ModifyGraph wbRGB=%s", wbRGB
	execute /q/z cmd
	sprintf cmd, "add_roi_controls()"
	execute /q/z cmd
end

/// area detector surveillance camera display
///
/// display an area detector channel in a simple image window
/// without any interactive controls.
///
/// @param	epicsname			base name of the detector, e.g. X03DA-SCIENTA:
///									image1: and cam1: are appended by the function.
///									see ad_connect().
///
/// @param	nickname			nick name under which this detector is referred to in Igor.
/// 								must be a valid name for a data folder.
///									see ad_connect().
///
/// @param	wbRGB				window background color, e.g. "(32768,49152,55296)"
///
function PearlCameraDisplay(epicsname, nickname, wbRGB)
	string epicsname
	string nickname
	string wbRGB
	
	string cmd
	sprintf cmd, "ad_connect(\"%s\", \"%s\")", epicsname, nickname
	execute /q/z cmd
	sprintf cmd, "display; appendimage root:pearl_epics:%s:image", nickname
	execute /q/z cmd
	sprintf cmd, "ModifyGraph wbRGB=%s", wbRGB
	execute /q/z cmd
	cmd = "ModifyGraph height={Plan,1,left,bottom}"
	execute /q/z cmd
end

/// display the angle scan tracker window
///
/// @param	epicsname			base name of the detector, e.g. X03DA-SCIENTA:
///									image1: and cam1: are appended by the function.
///									see ad_connect().
///
/// @param	wbRGB				window background color, e.g. "(32768,49152,55296)"
///
function PearlAnglescanTracker(epicsname, wbRGB)
	string epicsname
	string wbRGB
	
	string cmd
	sprintf cmd, "ast_setup()"
	execute /q/z cmd
	sprintf cmd, "ModifyGraph wbRGB=%s", wbRGB
	execute /q/z cmd
end

/// display the sample tracker window
///
/// @param	epicsname			base name of the detector, e.g. X03DA-SCIENTA:
///									image1: and cam1: are appended by the function.
///									see ad_connect().
///
/// @param	wbRGB				window background color, e.g. "(32768,49152,55296)"
///
function PearlSampleTracker(action)
	variable action
	string cmd
	sprintf cmd, "sample_tracker(%u)", action
	execute /q/z cmd
end

