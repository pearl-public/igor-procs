#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.1
#pragma ModuleName = PearlCompat
#pragma version = 1.01

// copyright (c) 2019 Paul Scherrer Institut
//
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//   http:///www.apache.org/licenses/LICENSE-2.0

/// @file
/// @brief compatibility procedures for igor 8
/// @ingroup ArpesPackage
///
///
/// the compatibility procedures ensure that igor experiments
/// created with the PEARL procedures under igor 8
/// can be opened with earlier igor versions (>= 6.34).
///
/// the following possible issues are addressed:
///
/// @arg length of object names

/// @namespace PearlCompat
/// @brief compatibility procedures for igor 8
///
/// PearlCompat is declared in @ref pearl-compat.ipf.


// Compatible CleanupName function
//
// Igor 8's CleanupName may return long object names (> 31 characters).
// This breaks compatibility with earlier Igor versions.
// Experiments that include waves, folders, windows etc. with long names
// cannot be loaded with an earlier version.
//
// This is a drop-in replacement function for CleanupName.
// In addition to the behaviour of CleanupName,
// this replacement ensures that names are limited to 31 characters.
//
// @param	name	object name to clean up
//
// @return		(str) clean object name
//
function /s PearlCleanupName(name)
	string name
	
#if IgorVersion() >= 8.00
	// note: this function is not threadsafe
	return CleanupName(name, 0, 31)
#else
	return CleanupName(name, 0)
#endif

end
