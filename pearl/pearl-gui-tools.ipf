#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.1
#pragma ModuleName = PearlGuiTools
#pragma version = 1.01

// Miscellaneous GUI tools
// * progress bar

// created: matthias.muntwiler@psi.ch, 2013-11-14
// Copyright (c) 2013 Paul Scherrer Institut
// $Id$

// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//   http://www.apache.org/licenses/LICENSE-2.0

function display_progress_panel(title, message, progress_max)
	string title
	string message
	variable progress_max

	NewPanel /K=1 /N=ProgressPanel /W=(200,200,402,260) as title
	TitleBox t_message,pos={2,2},size={189,13},title=message
	TitleBox t_message,frame=0
	ValDisplay vd_progress,pos={2,20},size={198,13}
	ValDisplay vd_progress,limits={0,progress_max,0},barmisc={0,0},mode= 3,value= _NUM:0
	Button b_abort,pos={74,38},size={50,20},title="Abort"
	DoUpdate /W=ProgressPanel /E=1
end

function update_progress_panel(progress, [message, progress_max])
	// returns true if the user clicked the Abort button
	variable progress
	string message
	variable progress_max

	if (!ParamIsDefault(message))	
		TitleBox t_message,title=message,win=ProgressPanel
	endif
	if (ParamIsDefault(progress_max))
		ValDisplay vd_progress,value=_NUM:progress,win=ProgressPanel
	else
		ValDisplay vd_progress,limits={0,progress_max,0},value=_NUM:progress,win=ProgressPanel
	endif
	DoUpdate /W=ProgressPanel
	return (v_flag == 2)
end

function kill_progress_panel()
	KillWindow ProgressPanel
end
