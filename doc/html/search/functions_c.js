var searchData=
[
  ['normalize_5fstrip_5f2d_956',['normalize_strip_2d',['../pearl-anglescan-process_8ipf.html#ac617c3b400488b656493af8ca08f1791',1,'pearl-anglescan-process.ipf']]],
  ['normalize_5fstrip_5fphi_957',['normalize_strip_phi',['../pearl-anglescan-process_8ipf.html#aaa734fddecdd75c7cabe20ba777b41b9',1,'pearl-anglescan-process.ipf']]],
  ['normalize_5fstrip_5ftheta_958',['normalize_strip_theta',['../pearl-anglescan-process_8ipf.html#a9b56897bd92d926d65f4c67bef1d41bb',1,'pearl-anglescan-process.ipf']]],
  ['normalize_5fstrip_5ftheta_5fscans_959',['normalize_strip_theta_scans',['../pearl-anglescan-process_8ipf.html#a992920d621023e6b483ff51eee68b508',1,'pearl-anglescan-process.ipf']]],
  ['normalize_5fstrip_5fthetaphi_960',['normalize_strip_thetaphi',['../pearl-anglescan-process_8ipf.html#ad0a93367d2e9b66bb7b81697e87adfaf',1,'pearl-anglescan-process.ipf']]],
  ['normalize_5fstrip_5fx_961',['normalize_strip_x',['../pearl-anglescan-process_8ipf.html#a48b7d774ed8d3f4329e9923e18e580e8',1,'pearl-anglescan-process.ipf']]],
  ['notebook_5fadd_5fattributes_962',['notebook_add_attributes',['../pearl-data-explorer_8ipf.html#a0c162346b59b0f66d34ee26ce5fe1e52',1,'pearl-data-explorer.ipf']]]
];
