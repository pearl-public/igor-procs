var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxy",
  1: "de",
  2: "p",
  3: "afmp",
  4: "abcdefghiklmnopqrstuw",
  5: "abcefhikmnopsuvwxy",
  6: "a",
  7: "aipt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Modules",
  7: "Pages"
};

