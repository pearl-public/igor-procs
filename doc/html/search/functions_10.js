var searchData=
[
  ['read_5fattribute_5finfo_1061',['read_attribute_info',['../pearl-area-import_8ipf.html#ac98a5f2d12b559aba4e53192c49a7743',1,'pearl-area-import.ipf']]],
  ['record_5fresults_1062',['record_results',['../fermi-edge-analysis_8ipf.html#aac6bac1ee0582caa0676bdc9c2d254f0',1,'fermi-edge-analysis.ipf']]],
  ['redim_5flinbg_5freduction_1063',['redim_linbg_reduction',['../pearl-scienta-preprocess_8ipf.html#a8e2aef3e0d5f2b304399a11423661fdc',1,'pearl-scienta-preprocess.ipf']]],
  ['reduce_5fbrick_5fworker_1064',['reduce_brick_worker',['../pearl-area-import_8ipf.html#a4efc9178892310c9e2caf40c61d71bd7',1,'pearl-area-import.ipf']]],
  ['reduce_5fslab_5fimage_1065',['reduce_slab_image',['../pearl-area-import_8ipf.html#a44d495fba0dd2b82dec13760a07fd226',1,'reduce_slab_image(wave slabdata, wave image, funcref reduction_func, string reduction_param):&#160;pearl-area-import.ipf'],['../pearl-pshell-import_8ipf.html#a8089a75744ffc3626305406e925d320a',1,'reduce_slab_image(wave slabdata, wave image, funcref reduction_func, string reduction_params):&#160;pearl-pshell-import.ipf']]],
  ['reduce_5fslab_5fworker_1066',['reduce_slab_worker',['../pearl-area-import_8ipf.html#a33f8faf117450af1d6dae9ef48786cd6',1,'reduce_slab_worker(funcref reduction_func):&#160;pearl-area-import.ipf'],['../pearl-pshell-import_8ipf.html#a33f8faf117450af1d6dae9ef48786cd6',1,'reduce_slab_worker(funcref reduction_func):&#160;pearl-pshell-import.ipf']]],
  ['rotate2d_5fx_1067',['rotate2d_x',['../pearl-vector-operations_8ipf.html#ac579a92f012f0d0ef7b8f097e1c8b3c7',1,'pearl-vector-operations.ipf']]],
  ['rotate2d_5fy_1068',['rotate2d_y',['../pearl-vector-operations_8ipf.html#a355150c423ab975fe7f1832917118ea3',1,'pearl-vector-operations.ipf']]],
  ['rotate_5fhemi_5fscan_1069',['rotate_hemi_scan',['../pearl-anglescan-process_8ipf.html#a5162488b366e217195d8f8bd7cdde0ce',1,'pearl-anglescan-process.ipf']]],
  ['rotate_5fx_5fwave_1070',['rotate_x_wave',['../pearl-vector-operations_8ipf.html#ada80428496dc748b960bd9c65df7da8b',1,'pearl-vector-operations.ipf']]],
  ['rotate_5fy_5fwave_1071',['rotate_y_wave',['../pearl-vector-operations_8ipf.html#adfd1d68e739694982fbd00b76568c1c0',1,'pearl-vector-operations.ipf']]],
  ['rotate_5fz_5fwave_1072',['rotate_z_wave',['../pearl-vector-operations_8ipf.html#a0030e927980581d57781ad391f2d872a',1,'pearl-vector-operations.ipf']]]
];
