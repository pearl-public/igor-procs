var fermi_edge_analysis_8ipf =
[
    [ "analyse_curved_edge", "fermi-edge-analysis_8ipf.html#a1c4a805435a1d43c2b6dfb6deb633894", null ],
    [ "FermiFuncLinDOS2D_corr", "fermi-edge-analysis_8ipf.html#a520d8de9fbc4276c19fb417861f05b0d", null ],
    [ "integrate_curved_edge", "fermi-edge-analysis_8ipf.html#a2a1d7b49c1f88f29ee6d49f6a6f4fbf8", null ],
    [ "record_results", "fermi-edge-analysis_8ipf.html#aac6bac1ee0582caa0676bdc9c2d254f0", null ],
    [ "show_shift", "fermi-edge-analysis_8ipf.html#acf72d644b8d37b6c26b1e070edba4e30", null ],
    [ "slit_correction", "fermi-edge-analysis_8ipf.html#a4cec596c8fd2b21953cb45d6d347211d", null ],
    [ "slit_shift", "fermi-edge-analysis_8ipf.html#a27f000c3a3ea74c49db31716be3396d4", null ],
    [ "hemi_radius_mm", "fermi-edge-analysis_8ipf.html#a0cb8da36beae05c79fe5b1da918d3897", null ],
    [ "mcp_radius_epass", "fermi-edge-analysis_8ipf.html#a4749b9bce3e1d0381bd9daeb97c9754c", null ],
    [ "mcp_radius_mm", "fermi-edge-analysis_8ipf.html#a4dcc00b93822f1663be2908b10d2ad3e", null ],
    [ "mcp_radius_pix", "fermi-edge-analysis_8ipf.html#a09f26b0a0fd940a3d8c6f92aa769c8bc", null ]
];