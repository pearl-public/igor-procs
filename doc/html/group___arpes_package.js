var group___arpes_package =
[
    [ "fermi-edge-analysis.ipf", "fermi-edge-analysis_8ipf.html", null ],
    [ "pearl-anglescan-panel.ipf", "pearl-anglescan-panel_8ipf.html", null ],
    [ "pearl-anglescan-process.ipf", "pearl-anglescan-process_8ipf.html", null ],
    [ "pearl-area-display.ipf", "pearl-area-display_8ipf.html", null ],
    [ "pearl-area-import.ipf", "pearl-area-import_8ipf.html", null ],
    [ "pearl-area-profiles.ipf", "pearl-area-profiles_8ipf.html", null ],
    [ "pearl-compat.ipf", "pearl-compat_8ipf.html", null ],
    [ "pearl-elog.ipf", "pearl-elog_8ipf.html", null ],
    [ "pearl-fitfuncs.ipf", "pearl-fitfuncs_8ipf.html", null ],
    [ "pearl-pmsco-import.ipf", "pearl-pmsco-import_8ipf.html", null ],
    [ "pearl-pshell-import.ipf", "pearl-pshell-import_8ipf.html", null ],
    [ "pearl-scienta-live.ipf", "pearl-scienta-live_8ipf.html", null ],
    [ "pearl-scienta-preprocess.ipf", "pearl-scienta-preprocess_8ipf.html", null ],
    [ "pearl-vector-operations.ipf", "pearl-vector-operations_8ipf.html", null ]
];