var pearl_menu_8ipf =
[
    [ "Display2dProfiles", "pearl-menu_8ipf.html#aad7d768680c6d8a9b8a7025c7e1ec75d", null ],
    [ "Display3dSlicer", "pearl-menu_8ipf.html#ac73a94f760455f19294a9f917b43f145", null ],
    [ "DisplayGizmoSlicer", "pearl-menu_8ipf.html#aab34952c2f3b36f9ee8619eb901ff581", null ],
    [ "LoadPearlArpes", "pearl-menu_8ipf.html#aa70ef420d6fe0f6a433cd2371fc4a03d", null ],
    [ "LoadPearlOptics", "pearl-menu_8ipf.html#af6c9740540c6242eb7bf57fc49de82ab", null ],
    [ "LoadPearlPreparation", "pearl-menu_8ipf.html#a3658ae687e12987fa1d70636849a060f", null ],
    [ "PearlAnglescanTracker", "pearl-menu_8ipf.html#a74bc5da7843ee6c25f2d9c93d22a6ffa", null ],
    [ "PearlCameraDisplay", "pearl-menu_8ipf.html#aab4ec7bc68f93029377b7657f40fbd6a", null ],
    [ "PearlLiveDisplay", "pearl-menu_8ipf.html#a61ded60be72959b00f22842afa37c56f", null ],
    [ "PearlMenuEnableFunc", "pearl-menu_8ipf.html#a3404a53bf13a01c1e811d1af6c35b726", null ],
    [ "PearlSampleTracker", "pearl-menu_8ipf.html#a1437f6baee0bd6d04bbcd2236c2d1f7f", null ]
];