var pearl_scienta_preprocess_8ipf =
[
    [ "capture_int_linbg_cursors", "pearl-scienta-preprocess_8ipf.html#ae6877c51ad15c2ba8a69c65356cb34b8", null ],
    [ "csr_int_linbg_reduction", "pearl-scienta-preprocess_8ipf.html#a95fbd22f52f61d2bff0625b7b8e159d1", null ],
    [ "gauss4_reduction", "pearl-scienta-preprocess_8ipf.html#a83cdbd96c5b59011914d53118e5ef71c", null ],
    [ "gauss6_reduction", "pearl-scienta-preprocess_8ipf.html#a11d42ef1352876666b710b7545360fce", null ],
    [ "int_linbg_reduction", "pearl-scienta-preprocess_8ipf.html#a1e91197cd7a3581b70bc59a194d3f43b", null ],
    [ "int_quadbg_reduction", "pearl-scienta-preprocess_8ipf.html#ad626526589efec3f2f72ad001702fe39", null ],
    [ "prompt_gauss4_reduction", "pearl-scienta-preprocess_8ipf.html#a1514250704b40aa2614d389a2e250d61", null ],
    [ "prompt_int_linbg_reduction", "pearl-scienta-preprocess_8ipf.html#a145c7275b8809c5e789b932ef46e4811", null ],
    [ "prompt_int_quadbg_reduction", "pearl-scienta-preprocess_8ipf.html#a6d06ea5a11ba79160efeea7fe673af8c", null ],
    [ "prompt_redim_linbg_reduction", "pearl-scienta-preprocess_8ipf.html#a6e7de6441bbcba217760448babaca827", null ],
    [ "redim_linbg_reduction", "pearl-scienta-preprocess_8ipf.html#a8e2aef3e0d5f2b304399a11423661fdc", null ],
    [ "test_gauss4_reduction", "pearl-scienta-preprocess_8ipf.html#adb78e8b2bbfd9c0faa5eb049b1dcad1c", null ]
];