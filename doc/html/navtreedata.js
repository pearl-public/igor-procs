/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "PEARL Procedures", "index.html", [
    [ "Introduction", "index.html", [
      [ "Introduction", "index.html#sec_intro", null ],
      [ "Installation", "index.html#sec_install", null ],
      [ "License Information", "index.html#sec_license", null ]
    ] ],
    [ "Projections", "_page_projections.html", null ],
    [ "Angle-scan processing", "pag_anglescan_processing.html", [
      [ "Introduction", "pag_anglescan_processing.html#sec_anglescan_intro", null ],
      [ "Data reduction", "pag_anglescan_processing.html#sec_import", [
        [ "Basic steps", "pag_anglescan_processing.html#sec_import_basics", null ],
        [ "Peak integration over linear background", "pag_anglescan_processing.html#sec_import_intlinbg", null ],
        [ "Peak fitting", "pag_anglescan_processing.html#sec_import_peakfit", null ],
        [ "Custom reduction functions", "pag_anglescan_processing.html#sec_import_custom", null ]
      ] ],
      [ "Normalization", "pag_anglescan_processing.html#sec_norm", [
        [ "Preparations", "pag_anglescan_processing.html#sec_norm_prep", null ],
        [ "Detector angle range", "pag_anglescan_processing.html#sec_norm_crop", null ],
        [ "Normalize detector angle", "pag_anglescan_processing.html#sec_norm_angle", null ],
        [ "Azimuthal variation (wobble)", "pag_anglescan_processing.html#sec_norm_wobble", null ],
        [ "Polar dependence", "pag_anglescan_processing.html#sec_norm_theta", null ]
      ] ],
      [ "Binning and plotting", "pag_anglescan_processing.html#sec_plot", [
        [ "Basic steps", "pag_anglescan_processing.html#sec_plot_basics", null ],
        [ "Refinements", "pag_anglescan_processing.html#sec_plot_refine", null ],
        [ "Interpolation", "pag_anglescan_processing.html#sec_plot_interp", null ],
        [ "Modulation function", "pag_anglescan_processing.html#sec_modulation", null ],
        [ "Projection", "pag_anglescan_processing.html#sec_projection", null ]
      ] ],
      [ "Data export", "pag_anglescan_processing.html#sec_export", [
        [ "Export picture", "pag_anglescan_processing.html#sec_export_plot", null ],
        [ "Export processed data", "pag_anglescan_processing.html#sec_export_data", null ]
      ] ]
    ] ],
    [ "Todo List", "todo.html", null ],
    [ "Packages", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", null ],
      [ "Variables", "functions_vars.html", null ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_page_projections.html",
"pearl-anglescan-tracker_8ipf.html#a33e84ae8e13f405d466b28e83f608cb9",
"pearl-elog_8ipf.html#acbba78d869a543edf7c2b80d7a8d2344"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';