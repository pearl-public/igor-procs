var struct_doniach_sunjic_struct =
[
    [ "broadening", "struct_doniach_sunjic_struct.html#ac9b18c8b44b43c2ee438f37f8d002a66", null ],
    [ "convolution", "struct_doniach_sunjic_struct.html#a7f05f7827435fea3c986a8d538496955", null ],
    [ "model", "struct_doniach_sunjic_struct.html#a02c13fdcf15e9adfee13464701bb7de2", null ],
    [ "oversampling", "struct_doniach_sunjic_struct.html#ab5a630be50286c3cf04e40d5880506e6", null ],
    [ "precision", "struct_doniach_sunjic_struct.html#a906e214875392bc470dbd4bb4bdda2db", null ],
    [ "pw", "struct_doniach_sunjic_struct.html#a92bbb374f66840510e7cb8b316057610", null ],
    [ "xdw", "struct_doniach_sunjic_struct.html#a750e7260bf5d4c936dadde714fb2db52", null ],
    [ "xw", "struct_doniach_sunjic_struct.html#a45c3a3fa68850032e545907ca65ab982", null ],
    [ "yw", "struct_doniach_sunjic_struct.html#a6cef648ad0cf4be1dd9fbe33ff5df1eb", null ]
];