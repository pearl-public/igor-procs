var structerror_code =
[
    [ "ALREADY_FILE_OPEN", "structerror_code.html#a19dc49bdfb4bd9601f17f907da158026", null ],
    [ "EMPTY_RESULTFILE", "structerror_code.html#ab7f29ef2ba8497c55f2bc55c4b9fc186", null ],
    [ "FILE_NOT_READABLE", "structerror_code.html#a71ce7c0413c44515d9570dab1ffd5ffd", null ],
    [ "INTERNAL_ERROR_CONVERTING_DATA", "structerror_code.html#afb49d1cffe8e7590892b018ac9e648cc", null ],
    [ "INVALID_RANGE", "structerror_code.html#a5477920df1edcc7a1af0513d9120947a", null ],
    [ "NO_FILE_OPEN", "structerror_code.html#affc9a8a46877373b0212d82d867ca5fa", null ],
    [ "NO_NEW_BRICKLETS", "structerror_code.html#a4ec3cbf922809b99b04d324d3a0bbb22", null ],
    [ "SUCCESS", "structerror_code.html#a36a53ca508600b841a54cfd3a3fd5402", null ],
    [ "UNKNOWN_ERROR", "structerror_code.html#a11b729058e2f4a2698ddaecf4e61c846", null ],
    [ "WAVE_EXIST", "structerror_code.html#aa91bd8ef7a635f4575161813ebb09f3b", null ],
    [ "WRONG_PARAMETER", "structerror_code.html#aa4279dfdaceed3bd57336cd4e38ed739", null ]
];