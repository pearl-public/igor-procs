this folder and subfolders contain the documentation of the PEARL procedures in HTML and PDF format.

the documentation is currently migrated to the doxygen format.
descriptions may be incomplete until the migration is finished.

to compile the documentation the following pieces of software are required:

doxygen:
http://www.doxygen.org/

gawk:
http://gnuwin32.sourceforge.net/packages/gawk.htm (windows)

doxygen-filter-ipf.awk:
http://www.igorexchange.com/project/doxIPFFilter

please see the original web sites for terms of use.
