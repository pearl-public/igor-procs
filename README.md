Introduction
============

PEARL Procedures is a suite of Igor Pro procedures developed for data acquisition and data processing at the PEARL beamline at the Swiss Light Source. PEARL Procedures requires Igor Pro 8 or newer.


Installation
============

PEARL Procedures should be installed according to the regular Igor Pro guidelines. Please read the Igor help `About Igor Pro User Files` for details.

- Make a `pearl-procs` directory in your private or shared `User Procedures` folder, and copy the PEARL Procedures distribution there.
- Create shortcuts of the `pearl-arpes.ipf` and `pearl-menu.ipf` files, and move them to the `Igor Procedures` folder next to your `User Procedures` folder.

Igor Pro 9 imports the HDF5 library by default. For earlier versions:

- Find the `HDF5.XOP` (`HDF5-64.xop` for 64-bit) extension in the `Igor Pro Folder` under `More Extensions/File Loaders` (`More Extensions (64-bit)/File Loaders`), create a shortcut, and move the shortcut to the `Igor Extensions` folder next to your `User Procedures` folder.
- Find the `HDF5 Help.ihf` next to `HDF5.XOP`, create a shortcut, and move the shortcut to the `Igor Help Files` folder next to your `User Procedures` folder.

PEARL Procedures are tested on Igor Pro 8.04, 64-bit.
Please make sure to use the latest release version of Igor Pro.


License
=======

The source code of PEARL Procedures is available under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) at <https://git.psi.ch/pearl-public/igor-procs>.
Please read and respect the respective license agreements.
Please acknowledge the use of the code and share your extensions with the original author.

Author
------

Matthias Muntwiler, <mailto:matthias.muntwiler@psi.ch>

Copyright
---------

Copyright 2009-2022 by [Paul Scherrer Institut](http://www.psi.ch)


Release Notes
=============

## rev-distro-3.0.0

- New panel and procedure interface for PShell data file import.
- Support for latest PShell file structure.
- Igor Pro 8.04 or later is required.

## rev-distro-2.2.0

- Updates, bugfixes and performance improvements in angle scan processing.

## rev-distro-2.1.0

- Check compatibility of major features with Igor 8.
- pshell-import does not apply a detector sensitivity scaling any more. The returned intensities have arbitrary units.

## rev-distro-2.0.3

- The interpolate_hemi_scan function now requires a projection argument unless stereographic projection is desired.

## rev-distro-2.0.0

- The interface of data reduction functions has changed to make data reduction more efficient in multi-peak fits. The supplied reduction functions and dialogs have been refactored. If you want to use your own reduction functions written for pre-2.0, you have to adapt them to the new interface.

